#include "Application.h"
#include <stdio.h>
#include "Rendering/VAO.h"
#include "Rendering/ObjLoader.h"
#include <string>
Application * g_pApplication = 0;



Application::Application(unsigned int width,unsigned int height): m_Width(width),m_Height(height)
{

	m_VAO = *ObjectLoader::LoadObject("../Resources/teapot.obj");
	//m_VAO2= *ObjectLoader::LoadObject("../Resources/triangle2.obj");

	//m_VAO.Print();
	SDL_Init(SDL_INIT_VIDEO);
	m_pWindow = SDL_CreateWindow("Reggae reggae rasterizer",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,m_Width,m_Height,SDL_WINDOW_SHOWN);
	m_pBuffer = new Framebuffer(m_Width,m_Height);


    m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED);
    m_ScreenRect.x = m_ScreenRect.y = 0;
    m_ScreenRect.w = m_Width;
    m_ScreenRect.h = m_Height;

    m_Renderer.SetActiveBuffer(m_pBuffer);


   	m_X = m_Y= 0;
   	m_Z      = -50;
   	m_Fov    = 45.0f*(M_PI/180.0f);
   	m_Angle  = 0;
   	m_Scale  = 1;
 
}


Application::~Application()
{
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
	if( m_pBuffer ) delete m_pBuffer;
}


void Application::Run()
{
	bool running = true;

	while( running )
	{
		SDL_Event runEvent;

		while( SDL_PollEvent(&runEvent) )
		{
			if ( runEvent.type == SDL_QUIT )
				running = false;
		}
		Update();
		Render();

		//running = false;
	}	
}



void Application::Update()
{
	const Uint8 * keys = SDL_GetKeyboardState(NULL);

//	if( keys[SDL_SCANCODE_W] ) m_Rasterizer.SetMode(Rasterizer::keLines);
//	if( keys[SDL_SCANCODE_F] ) m_Rasterizer.SetMode(Rasterizer::keFill);

	if( keys[SDL_SCANCODE_W] ) m_Y += 0.1f;
	if( keys[SDL_SCANCODE_S] ) m_Y -= 0.1f;
	if( keys[SDL_SCANCODE_A] ) m_X -= 0.1f;
	if( keys[SDL_SCANCODE_D] ) m_X += 0.1f;
	if( keys[SDL_SCANCODE_Z] ) m_Z -= 0.1f; 
	if( keys[SDL_SCANCODE_X] ) m_Z += 0.1f; 

	if( keys[SDL_SCANCODE_P]) m_Scale+= 0.1f;
	if( keys[SDL_SCANCODE_O]) m_Scale-= 0.1f;
	
	if( keys[SDL_SCANCODE_Q]) m_Angle+= 1.0f;
	if( keys[SDL_SCANCODE_E]) m_Angle-= 1.0f;

}

void Application::Render()
{

	m_pBuffer->Clear();



	const Uint8 * keys = SDL_GetKeyboardState(NULL);

	bool apply = false;
	if( keys[SDL_SCANCODE_K] ) apply = true;

	Uint32 amask = 0xff000000;
    Uint32 bmask = 0x00ff0000;
    Uint32 gmask = 0x0000ff00;
    Uint32 rmask = 0x000000ff;

   // float axis[] = {0,1,0};
    float data[] = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
    Mat4f matrix(data);    
    m_Renderer.SetProjectionMatrix( Perspective<float>(m_Fov,800.0f/640.0f,0.1f,100.0f) );

    //m_Renderer.SetProjectionMatrix( Mat4f(data) ); //Identity
	//matrix *= Scale<float>(m_Scale,m_Scale,m_Scale);
	//matrix *= Translate<float>(0.5f,0.5f,0.0f);	
	//matrix *= Rotate<float>(m_Angle *(M_PI/180.0f),Vector3f(axis));
	//matrix *= Translate<float>(-0.5f,-0.5f,0.0f);
   	matrix *= Translate<float>(m_X,m_Y,m_Z);



    m_Renderer.SetModelMatrix(matrix);
	m_Renderer.Render(m_VAO);


    SDL_Surface * surface = SDL_CreateRGBSurfaceFrom(m_pBuffer->GetData(), m_Width, m_Height, 32, m_Width*4, rmask, gmask, bmask, amask);
    SDL_Texture * texture = SDL_CreateTextureFromSurface(m_pRenderer, surface);
    SDL_RenderClear(m_pRenderer);
    SDL_RenderCopyEx(m_pRenderer, texture, NULL, &m_ScreenRect,0,NULL,(SDL_RendererFlip)(SDL_FLIP_VERTICAL));
    SDL_RenderPresent(m_pRenderer);
    
    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);

}



void Application::InitApplication(unsigned int width,unsigned int height)
{
	g_pApplication = new Application(width,height);
}


void Application::DestroyApplication()
{
	if( g_pApplication ) delete g_pApplication;
}