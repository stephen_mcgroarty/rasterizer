#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>

#define ELOG printf

typedef unsigned char Byte;
typedef float FLOAT;

struct RGBColour
{
	Byte red,green,blue,alpha;
};

const RGBColour BLACK  = { 0x00, 0x00, 0x00,0xFF};
const RGBColour RED    = { 0xFF, 0x00, 0x00,0xFF};
const RGBColour BLUE   = { 0x00, 0x00, 0xFF,0xFF};
const RGBColour GREEN  = { 0x00, 0xFF, 0x00,0xFF};
const RGBColour WHITE  = { 0xFF, 0xFF, 0xFF,0xFF};

#endif