#ifndef APPLICATION_H
#define APPLICATION_H
#include "SDL.h"
#include "Maths/Maths.h"
#include "Rendering/Renderer.h"
#include <vector>
#include "Rendering/VAO.h"

class Application
{
public:

	static void InitApplication(unsigned int width,unsigned int height);
	static void DestroyApplication();

	void Run();

private:

	unsigned int m_Width,m_Height;
	void Update();
	void Render();


	void RenderBufferToScreen();


	Application(unsigned int width,unsigned int height);
	~Application();
	SDL_Window *m_pWindow;

	Framebuffer *m_pBuffer;

	Renderer m_Renderer;
	VAO m_VAO,m_VAO2;
	std::vector<std::pair<float,float> > m_Clicks;

	float m_X,m_Y,m_Z;
	float m_Fov;
	float m_Scale;
	float m_Angle;
	SDL_Renderer* m_pRenderer;
    SDL_Rect m_ScreenRect;
};



extern Application * g_pApplication;
#endif