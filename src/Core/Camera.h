#ifndef CAMERA_H
#define CAMERA_H

#include "Maths/Maths.h"

class Camera
{
public:
	Camera(const Vector3f & eye,const Vector3f & at,const Vector3f & up): m_Eye(eye), m_At(at), m_Up(up) { }


	Mat4f GetProjection() const;

private:

	Vector3f m_Eye,m_At,m_Up;

};

#endif