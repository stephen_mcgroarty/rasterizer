#ifndef SHADER_H
#define SHADER_H
#include "GraphicsUtils.h"
#include "Core/Common.h"
#include "Rendering/Framebuffer.h"
#include <cmath>
#include <time.h>
/* 
	File of shader function declarations (defs in Shaders.cpp) and uniform data
	
	VS = VertexShader 
	FS = Fragment Shader 
*/


class Shader
{
public:
	static ShaderData g_Uniforms;
	virtual ~Shader(){}
};




class VertexShader: public Shader
{ 
public: 
	virtual void Execute(const Vertex & inVert,ShaderData&) = 0;
	virtual ~VertexShader() {};
	Vertex g_OutVert;
};


class FragmentShader: public Shader
{
public: 
	virtual void Execute(Framebuffer * buffer,const Fragment& inFragment) = 0;
	virtual ~FragmentShader() {};

};


class VS_Basic: public VertexShader
{
public:
	void Execute(const Vertex & inVert,ShaderData & output)
	{
		Vector4f vert = inVert.GetPosition();

		//output.vectorInput.(Vector4f(1.0f,1.0f,0.0f,0.0f));
		vert = g_Uniforms.matrixInput[1]*vert;
		vert = g_Uniforms.matrixInput[0]*vert;
		g_OutVert = Vertex(vert);
	}
};
/*
uniform vec4 LightDir;
uniform vec3 LightIntensity;



vec3 phongShade(vec3 colour)
{
	vec3 v = normalize( -eyeCoords.xyz );
	vec3 s;
	
	if( LightDir.w == 0.0 ) //if directional light
		s = normalize( vec3(LightDir) );
	else 
		s = normalize(vec3(LightDir.xyz - eyeCoords));

	float sDotN = max( dot(s,out_Normal),0.0); 
	vec3 h = normalize( v + s);

	vec3 i = colour*0.1;								   					//ambient
	i += colour* 0.7 * sDotN;					 							//diffuse

	if( sDotN > 0.0 ) i += colour * pow(max( dot(h,out_Normal),0.0),2) * LightIntensity; //specular

	return i;
}*/



class VS_Flat: public VertexShader
{
	void Execute(const Vertex & inVert,ShaderData & output)
	{
		Vector3f lightDir(0.0f,0.0f,1.0f);


		Vector4f vert = inVert.GetPosition();
		Vector3f normal = Mat3f(g_Uniforms.matrixInput[1])*inVert.GetNormal();
		normal.Normalize();

		vert = g_Uniforms.matrixInput[1]*vert;
		vert = g_Uniforms.matrixInput[0]*vert;

		float lightDotNormal = lightDir.Dot(normal);
		if( lightDotNormal < 0.0f) lightDotNormal = 0.0f;


		//Base colour AKA ambient
		Vector4f baseColour(1.0f,0.0f,0.0f,1.0f);
		Vector4f outColour = baseColour*0.2f;


 		outColour += baseColour*0.6f*lightDotNormal;
 		outColour += baseColour*pow(max( lightDir.Dot(normal),0.0f),2)*0.7f;

		outColour.GetData()[3] = 1.0f;
		output.vectorInput["colour"] = (outColour);
		
		g_OutVert = Vertex(vert);
	}
};


class FS_Barycentric: public FragmentShader
{
public:

	void Execute(Framebuffer * buffer,const Fragment& inFragment)
	{
		Vector4f colVec = Vector4f(inFragment.baryX,inFragment.baryY,0.0f,1.0f);
	//	colVec.Print();
		RGBColour colour = Vector4fToRGBColour(colVec);
		buffer->SetPixel(inFragment.x,inFragment.y,colour);
	}
};



class FS_Basic: public FragmentShader
{
public:

	void Execute(Framebuffer * buffer,const Fragment& inFragment)
	{
		Vector4f colVec = inFragment.data.vectorInput.at("colour");
		RGBColour &&colour = Vector4fToRGBColour(colVec);
		buffer->SetPixel(inFragment.x,inFragment.y,colour);
	}

};





#endif