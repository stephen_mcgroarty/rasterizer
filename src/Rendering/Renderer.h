#ifndef RENDERER_H
#define RENDERER_H
#include "Rendering/Vertex.h"
#include <vector>
#include "Rendering/VAO.h"
#include "Rendering/Framebuffer.h"
#include "Rendering/Rasterizer.h"
#include "Rendering/GraphicsUtils.h"
#include "Rendering/Shaders.h"

class Renderer
{
public:

	Renderer();
	~Renderer();
	void Render(const VAO & vao);

	void SetModelMatrix(const Mat4f & mat);
	void SetProjectionMatrix(const Mat4f mat);

	void SetActiveBuffer(Framebuffer * buffer) { m_pActiveBuffer = buffer; m_Rasterizer.m_pFramebuffer = m_pActiveBuffer; }



	Framebuffer * GetActiveBuffer() const { return m_pActiveBuffer; }
private:
	VertexShader   * m_pVertexShader;
	FragmentShader * m_pFragmentShader;

	//void (*m_pVertexShader)(const Vector4f & inVert,Vertex_Output&);
	//void (*m_pFragmentShader)(const Vertex_Output&,Framebuffer*,const Fragment&);

	//std::vector<Vertex> m_VertexBuffer;
	//std::vector<Primitive> m_PrimitiveBuffer;

	Rasterizer m_Rasterizer;
	Framebuffer *m_pActiveBuffer;

	//Takes in VAO outputs vector of vertices
	void VertexProcess(const VAO & vao);

	//Performs vertex post process (aka primitive assembly)
	void VertexPostProcess();

	//Perfoms rasterization, aka converts vertices into 2d array of fragment buckets
	void Rasterize(FragmentBucket &);

	//Turns the 2d array of fragment buffers into a 2D image (via framebuffer)
	void FragmentProcess(FragmentBucket &);

};

#endif