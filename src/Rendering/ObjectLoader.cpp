#include "ObjLoader.h"
#include <fstream>
#include <sstream>
#include <Maths/Maths.h>


void ObjectLoader::RemoveSlashes(std::string & s)
{
	while(s.find("/") != std::string::npos)
			s.replace(s.find("/"),1," ");
}

FaceFormat ObjectLoader::HandleFace(const std::string& txt)
{
	if( txt.find("/") == std::string::npos )
		return V_ONLY;
	if( txt.find("//") != std::string::npos)
		return VN;
	if( txt.find("/",txt.find("/")+1) != std::string::npos)
		return VTN;

	return VT;

}

void ObjectLoader::GenerateNormals(const std::vector<float> & verts,std::vector<float> & normals)
{
	for(unsigned int i =0; i < verts.size(); i+=9)
	{
		Vector3f p1(&verts[i]);
		Vector3f p2(&verts[i+3]);

		Vector3f result = p1.Cross(p2);
		result.Normalize();

		normals.push_back(result.GetData()[0]);
		normals.push_back(result.GetData()[1]);
		normals.push_back(result.GetData()[2]);
	}
}

VAO * ObjectLoader::LoadObject(const std::string & filePath)
{
	// load file into stream for efficency
	std::ifstream file(filePath,std::ios::binary | std::ios::beg | std::ios::in);
	std::stringstream stream;

	stream << file.rdbuf();

	file.close();

	std::vector<Vertex> vertices;
	std::vector<Vector3f> normals;
	std::vector<unsigned int> vertexIndices;
	std::vector<unsigned int> normalIndices;



	FaceFormat format = FORMAT_NULL;

	for(std::string itr = ""; stream.good(); stream >> itr )
	{
		if( itr.compare("") == 0 ) continue;


		if(itr[0] == 'v')
		{

			if(itr[1] == 'n')
			{
				float data[3];
				stream >> data[0] >> data[1] >> data[2];
				normals.push_back(Vector3f(data));
			}
			else if(itr[1] == 't')
			{

			}
			else 
			{
				float data[3];
				stream >> data[0] >> data[1] >> data[2];
				vertices.push_back(Vertex(Vector3f(data)));
			}

		} else if(itr[0] == 'f')
		{
			std::string indiceOne,indiceTwo,indiceThree;

			//Load the three indice sets
			stream >> indiceOne >> indiceTwo >> indiceThree;

			//If the face format isn't set
			if( format == FORMAT_NULL) 
				format = HandleFace(indiceOne);

			//Remove the slashes leaving just the individual indices
			RemoveSlashes(indiceOne);
			RemoveSlashes(indiceTwo);
			RemoveSlashes(indiceThree);

			//Add the indice sets (with the slashes removed) back into a buffer
			std::stringstream buffer;
			buffer << indiceOne << " " << indiceTwo << " " << indiceThree;

			//Loop over the buffer parsing each set of indices
			while(buffer.good())
			{
				if( format == VTN )
				{
					unsigned int index[3];
					buffer >> index[0] >> index[1] >> index[2];

					vertexIndices.push_back(index[0]-1);
					//UVs go here
					normalIndices.push_back(index[2]-1);
				} 
					else if( format == V_ONLY )
				{
					unsigned int i;
					buffer >> i;
					vertexIndices.push_back(i-1);	
				} 
					else if( format == VN )
				{
					unsigned int index[2];
					buffer >> index[0] >> index[1];
					vertexIndices.push_back(index[0]-1);
					normalIndices.push_back(index[1]-1);

				}
					else if( format == VT )
				{
					unsigned int index[2];
					buffer >> index[0] >> index[1];
					vertexIndices.push_back(index[0]-1);
				}	
			}
		} else
		{
			char ch[256];
			stream.getline(ch,256);
		}
		
	}

	std::vector<Vertex> outVerts;


	//If model is not indexed just return vertices as is
	if( vertexIndices.size() == 0 )
	{
		printf("Loaded object with %lu vertices\n",vertices.size() );
		return new VAO(vertices);
	}



	for(unsigned int indiceIndex =0; indiceIndex < vertexIndices.size(); ++indiceIndex)
	{
		unsigned int vertexIndice = vertexIndices[indiceIndex];
		unsigned int normalIndice;

		if( normalIndices.size() != 0 ) normalIndice = normalIndices[indiceIndex]; 
		else normalIndice =0;

		outVerts.push_back( vertices[vertexIndice]);

		if( normalIndices.size() > normalIndice)
		{
			outVerts.back().SetNormal(normals[normalIndice]);
		}
	}

	printf("Vertices %lu indices %lu normal indices %lu out vertices %lu \n",vertices.size(),vertexIndices.size(),normalIndices.size(),outVerts.size() );


//	return new VAO(&outVerts[0],outVerts.size(),&outUv[0],outUv.size(),&outIndices[0],outIndices.size(),&outNorms[0],outNorms.size());

	return new VAO(outVerts);

}