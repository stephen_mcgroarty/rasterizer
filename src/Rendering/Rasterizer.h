#ifndef RASTERIZER_H
#define RASTERIZER_H
#include "Rendering/Framebuffer.h"
#include "Maths/Maths.h"
#include "Rendering/Vertex.h"
#include "Rendering/GraphicsUtils.h"
#include <tuple>


class Rasterizer
{
public:

	Rasterizer();

	enum DrawMode
	{
		keFill  = 0x00,
		keLines = 0x01
	};

	void DrawLine(const Vector4f & pointOne,const Vector4f & pointTwo) const;

	void Render(FragmentBucket & fragmentBucket,Primitive & Primitive) const;


	void SetMode(const DrawMode mode)  { m_Mode = mode; }

	Framebuffer * m_pFramebuffer;
private:

	bool CalculateBarycentricCoords(Fragment & frag,const Vector4f & v1,const Vector4f & v2,const Vector4f & v3) const;
	DrawMode m_Mode;
	void Rasterize(FragmentBucket & fragmentBucket,
					const Vector4f & A,
					const Vector4f & B,
					const Vector4f & C,
					const Primitive & primitive) const;


	struct BoundingBox
	{
		BoundingBox(const Vector4f && tr,
					const Vector4f && br,
					const Vector4f && bl,
					const Vector4f && tl): topRight(std::move(tr)),bottomRight(std::move(br)),bottomLeft(std::move(bl)),topLeft(std::move(tl)){}

		Vector4f topRight,bottomRight,bottomLeft,topLeft;
	};

	//Build the bounding box around the cube
	const BoundingBox BuildBoundingBox(const Vector4f & A,const Vector4f & B,const Vector4f & C) const;

	void RenderWireframe(FragmentBucket & fragmentBucket,const Vector4f * A,const Vector4f * B, const Vector4f * C) const;

	//Reorder the elements by their Y-axis cords, where A < B < C, A < B =C, ect 
	void Reorder(Primitive & primitive) const;
};

#endif