#include "Rasterizer.h"
#include <stdio.h>
#include <Core/Common.h>
#include <utility>

Rasterizer::Rasterizer():m_Mode(keFill) {}



void Rasterizer::Reorder(Primitive & primitive) const
{
	if( primitive.one.GetPosition()[1] < primitive.two.GetPosition()[1] )
	{
		std::swap(primitive.one,primitive.two);
	}

	if( primitive.one.GetPosition()[1] < primitive.three.GetPosition()[1])
	{
		std::swap(primitive.one,primitive.three);

	}

	if( primitive.two.GetPosition()[1] < primitive.three.GetPosition()[1] )
	{	
		std::swap(primitive.two,primitive.three);
	}
}	

//From Maths textbook
//
//P = w0p0 + w1p1 + w2p2
//Sub in w0 = 1 - w1 - w2
//P = (1 - w1 - w2)p0 + w1p1 + w2p2
//P = p0 - p0w1 - p0w2 + w1p1 + w2p2
//P = p0 + (p1 - p0)w1 + (p2 - p0)w2
//P - p0 = (p1 - p0)w1 + (p2 - p0)w2
//Sub R = P - p0, Q1 = (p1 - p0), Q2 = p2 - p0
//R = Q1w1 + Q2w2
//Dot product with Q1 : R.Q1 = w1Q1^2 + w2Q1Q2
//Dot product with Q2 : R.Q2 = w1Q1Q2 + w2Q2^2
//
// Which in matrix form is
// |R.Q1|   |Q1^2  Q1Q2|   |w1|
// |    | = |          | * |  |
// |R.Q2|   |Q1Q2  Q2^2|   |w2|

// Inversed
// 
// |w1|	   			1			 |  Q2^2 -Q1Q2 |   | R.Q1 |
// |  | = ---------------------- |		       | * |      |
// |w0|  (Q1^2)(Q2^2) - (Q1Q2)^2 | -Q1Q2  Q1^2 |   | R.Q2 |
//
// Det = (1/(Q1^2)(Q2^2) - (Q1Q2)^2)
// |w1|	 |  Det*Q2^2 Det*-Q1Q2 |   | R.Q1 |
// |  | =|		               | * |      |
// |w2|  | Det*-Q1Q2  Det*Q1^2 |   | R.Q2 |
//
// |w1|	 |  Det*Q2^2 Det*-Q1Q2 |   | R.Q1 |
// |  | =|		               | * |      |
// |w2|  | Det*-Q1Q2  Det*Q1^2 |   | R.Q2 |
//
// M00 = Det*Q2^2
// M10 = Det*-Q1Q2 
// M01 = Det*-Q1Q2
// M11 = Det*Q1^2
// |w1|	  | R.Q1*M00 + R.Q2*M10 |
// |  | = |                     |
// |w2|   | R.Q1*M01 + R.Q2*M11 |

// w1 = R.Q1*M00 + R.Q2*M10
// w2 = R.Q1*M01 + R.Q2*M11 
// w3 = 1 - w1 - w2
bool Rasterizer::CalculateBarycentricCoords(Fragment & frag,
											const Vector4f & v1,
											const Vector4f & v2,
											const Vector4f & v3) const
{
	//Sub R = P - p0, Q1 = (p1 - p0), Q2 = p2 - p0
	// Det = (1/(Q1^2)(Q2^2) - (Q1Q2)^2)
	Vector3f P  = Vector3f(frag.x,frag.y,frag.z);
	Vector3f R  = P - (v1);
	Vector3f Q1 = (Vector3f(v2) - Vector3f(v1));
	Vector3f Q2 = (Vector3f(v3) - Vector3f(v1));

	float Q2DotQ2 = Q2.Dot(Q2);
	float Q1DotQ1 = Q1.Dot(Q1);
	float Q1DotQ2 = Q1.Dot(Q2);


	// Det = (1/(Q1^2)(Q2^2) - (Q1Q2)^2)
	float determinant = 1 / ((Q1DotQ1*Q2DotQ2) - (Q1DotQ2*Q1DotQ2));

	float M00 = determinant*Q2DotQ2;
	float M10 = determinant*(-Q1DotQ2);
	float M01 = determinant*(-Q1DotQ2);
	float M11 = determinant*Q1DotQ1;

	float RDotQ1 = R.Dot(Q1);
	float RDotQ2 = R.Dot(Q2);

	frag.baryX = RDotQ1*M00 + RDotQ2*M10;
	frag.baryY = RDotQ1*M01 + RDotQ2*M11;
	frag.baryZ = 1 - frag.baryX - frag.baryY;

	if( frag.baryX < 0.0f || frag.baryY < 0.0f || frag.baryZ < 0.0f)
	{
		return false;
	}
	return true;
}




const Rasterizer::BoundingBox Rasterizer::BuildBoundingBox(const Vector4f & A,const Vector4f & B,const Vector4f & C)const
{


	int maxX = A[0];
	int minX = A[0];


	//Sort by X
	if( minX > B[0] ) minX = B[0];
	if( minX > C[0] ) minX = C[0];

	if( maxX < B[0] ) maxX = B[0];
	if( maxX < C[0] ) maxX = C[0];


	//Already sorted by Y
	int maxY = A[1];
	int minY = C[1];

	return {Vector4f(maxX,maxY),Vector4f(maxX,minY),Vector4f(minX,minY),Vector4f(minX,maxY)};
}




//TODO: Optimize algorithm by removing the pointless Barycentric calculations around square and
// calculate barycentric coord at A and B and then interpolate them. The use usuall scanline algorithm
void Rasterizer::Rasterize(FragmentBucket & fragmentBucket,
					const Vector4f & A,
					const Vector4f & B,
					const Vector4f & C,
					const Primitive & primitive) const
{

	const BoundingBox & box = BuildBoundingBox(A,B,C);




	int top = box.topRight[1];
	int bottom = box.bottomRight[1];

	int left = box.topLeft[0];
	int right= box.topRight[0];

	for(int scanline = top; scanline >= bottom && scanline > 0; --scanline)
	{

		if( scanline < 0 || scanline >= m_pFramebuffer->GetHeight() ) continue;


		float gradient1 = A[1] != B[1] ? (scanline - A[1]) / (B[1] - A[1]) : 1 ;	
		float gradient2 = B[1] != C[1] ? (scanline - C[1]) / (B[1] - C[1]) : 1 ;	


		float z1 = interpolate(A[3],B[3],gradient1);
		float z2 = interpolate(A[3],C[3],gradient2);

		bool rightHandSide = false;
		for(int xScan = left; xScan <= right; ++xScan)
		{

			if( xScan < 0 || xScan >= m_pFramebuffer->GetWidth() || left == right) continue;

			float gradient = (xScan - left) / (right - left);
			float z = interpolate(z1,z2,gradient);


			if( z < m_pFramebuffer->m_pDepthBuffer[xScan][scanline] )
				continue;

			fragmentBucket.push_back(Fragment());
			Fragment & frag = fragmentBucket.back();
			frag.x = xScan;
			frag.y = scanline;
			frag.z = 1;

			if( !CalculateBarycentricCoords(fragmentBucket.back(),A,B,C) || frag.x < 0 || frag.y < 0  )
			{
				 fragmentBucket.pop_back();


				 //If we have reached the right hand side of the triangle break
				 if( rightHandSide) 
				 	break;


				 //Skip adding to depth buffer
				 continue;
			}

			rightHandSide = true;

			m_pFramebuffer->m_pDepthBuffer[xScan][scanline] = z;
		}
	}
}



void Rasterizer::Render(FragmentBucket & fragmentBucket,Primitive & primitive) const
{
	const Mat4f & screenSpaceMat = m_pFramebuffer->GetBufferCoordMatrix();
	Vector4f & t = primitive.one.GetPosition(),
							&m = primitive.two.GetPosition(), 
								&b = primitive.three.GetPosition();


	float z1  = 1.0f/t[3];
	float z2  = 1.0f/m[3];
	float z3  = 1.0f/b[3];

	//Perspective correction AKA divide by W
	t[0] /= t[3];
	t[1] /= t[3];
	t[2] /= t[3];
	t[3] /= t[3];

	m[0] /= m[3];
	m[1] /= m[3];
	m[2] /= m[3];
	m[3] /= m[3];

	b[0] /= b[3];
	b[1] /= b[3];
	b[2] /= b[3];
	b[3] /= b[3];



	//Screenspace transform
	t = screenSpaceMat*t;
	m = screenSpaceMat*m;
	b = screenSpaceMat*b;

	t[3] = z1;
	b[3] = z2;
	m[3] = z3;
	
	Reorder(primitive);
	if( m_Mode == keFill)
	{
		Rasterize(fragmentBucket,primitive.one.GetPosition(),primitive.two.GetPosition(),primitive.three.GetPosition(),primitive);
	} 
		else if( m_Mode == keLines)
	{
//		RenderWireframe(fragmentBucket,top,middle,bottom);
	}

}




