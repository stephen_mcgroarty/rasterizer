#ifndef GRAPHICS_UTILS
#define GRAPHICS_UTILS
#include "Vertex.h"
#include <vector>
#include <map>
#include <string>



//Data to and from the shader
struct ShaderData
{
	ShaderData() { }
	ShaderData(const std::map<std::string,Vector4f> & v): vectorInput(v) { }

	std::map<std::string,Vector4f> vectorInput;
	std::vector<Mat4f> matrixInput;

	ShaderData operator+(const ShaderData & rhs)
	{
		ShaderData temp = *this;
		
		auto itrOne = temp.vectorInput.begin();
		auto itrTwo = rhs.vectorInput.begin();

		for(; itrOne != temp.vectorInput.end() && itrTwo != rhs.vectorInput.end();
			 ++itrOne,++itrTwo)
		{
			itrOne->second += itrTwo->second;	
		}
		return temp;
	}


	ShaderData operator*(float scalar)
	{
		ShaderData temp = *this;
		for(auto itr =  temp.vectorInput.begin(); itr != temp.vectorInput.end(); ++itr)
		{
			itr->second *= scalar;
		}	

		return temp;
	}
};


struct Fragment
{
	int x,y;
	float z;

	//Barycentric coordinates
	float baryX,baryY,baryZ;

	ShaderData data;
};

struct Primitive
{
	Primitive(const Vertex & v1,const Vertex & v2,const Vertex &v3): one(v1),two(v2),three(v3){}

	void SetShaderData(	const ShaderData & d1,
						const ShaderData & d2,
						const ShaderData & d3)
	{
		dataOne  = d1;
		dataTwo  = d2;
		dataThree= d3;
	}

	ShaderData Interpolate(float x,float y,float z)
	{
		return dataOne*x + dataTwo*y + dataThree*z;
	}
	Vertex one,two,three;
	ShaderData dataOne,dataTwo,dataThree;
};



//Fragment buffer is the vector of fragments on any given pixel
typedef std::vector<Fragment> FragmentBucket;



#endif