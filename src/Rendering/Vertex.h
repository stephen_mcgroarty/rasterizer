#ifndef VERTEX_H
#define VERTEX_H
#include "Maths/Maths.h"
#include <vector>
class Vertex
{
public:
	Vertex() { }
	Vertex(const Vector4f & position);
	Vertex(const Vector3f & position);


	inline void SetNormal(const Vector3f & normal) { m_Normals = normal; }

	void Print()
	{
		printf("Position:"); m_Position.Print();
		printf("Normal:");   m_Normals.Print();
	}

	inline Vector4f & GetPosition() { return m_Position;}
	inline Vector3f & GetNormal()   {return m_Normals;}

	inline const Vector4f & GetPosition() const{return m_Position;}
	inline const Vector3f & GetNormal()   const{return m_Normals;}
private:
	Vector4f m_Position;
	Vector3f m_Normals;




};

#endif