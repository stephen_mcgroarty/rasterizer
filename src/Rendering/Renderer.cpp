#include "Rendering/Renderer.h"
#include <time.h>
#include "Rasterizer.h"


Renderer::Renderer()
{
	//m_pVertexShader  = new VS_Basic;
	//m_pFragmentShader= new FS_Barycentric;

	m_pVertexShader = new VS_Flat;
	m_pFragmentShader=new FS_Basic;
}


Renderer::~Renderer()
{
	if( m_pVertexShader  ) delete m_pVertexShader;
	if( m_pFragmentShader) delete m_pFragmentShader;
}


void Renderer::SetModelMatrix(const Mat4f & mat) 
{
	if( Shader::g_Uniforms.matrixInput.size() == 1)
		Shader::g_Uniforms.matrixInput.push_back(mat);
	else 
		Shader::g_Uniforms.matrixInput[1] = mat;
}



void Renderer::SetProjectionMatrix(const Mat4f mat) 
{
	if( Shader::g_Uniforms.matrixInput.size() == 0)
		Shader::g_Uniforms.matrixInput.push_back(mat);
	else
		Shader::g_Uniforms.matrixInput[0] = mat;
}



void Renderer::Render(const VAO & vao)
{
	const unsigned int NUMBER_VERTICES_PER_PRIMITIVE  = 3;
	unsigned int numFragments = 0;


	clock_t totalEnd= 0,totalVertex =0,totalRaster = 0,totalFragment = 0;
	//For each vertex (grouped into primatives) in the VAO
	for(unsigned int index = 0; 
		index < vao.GetVertices().size(); 
		index += NUMBER_VERTICES_PER_PRIMITIVE)
	{

		clock_t start = clock();

		//Initalize the data to be returned from the Vertex Shader
		Vertex vertexBatch[NUMBER_VERTICES_PER_PRIMITIVE] ;
		ShaderData vertexOutputs[NUMBER_VERTICES_PER_PRIMITIVE];

		//For each vertex in the primative set
		for(unsigned int vertexIndex = index; 
				vertexIndex < index +NUMBER_VERTICES_PER_PRIMITIVE; 
					++vertexIndex)
		{
			Vertex vert = vao.GetVertices()[vertexIndex];
			//if( vertexIndex-index == 0) vertexOutputs[vertexIndex - index].vectorInput["colour"] = Vector4f(1.0f,0.0f,0.0f,1.0f);
			//if( vertexIndex-index == 1) vertexOutputs[vertexIndex - index].vectorInput["colour"] = Vector4f(0.0f,1.0f,0.0f,1.0f);
			//if( vertexIndex-index == 2) vertexOutputs[vertexIndex - index].vectorInput["colour"] = Vector4f(0.0f,0.0f,1.0f,1.0f);

			//Run vertex Shader
			m_pVertexShader->Execute(vert,vertexOutputs[vertexIndex - index]);

			vertexBatch[vertexIndex - index] = m_pVertexShader->g_OutVert;
		}	

		clock_t vertexTime = clock() -start;


		clock_t rasterStart = clock();
		//Vertex post processing, turn the vertices into primitives
		Primitive primitive(vertexBatch[0], vertexBatch[1], vertexBatch[2]);
		primitive.SetShaderData(vertexOutputs[0],vertexOutputs[1],vertexOutputs[2]);

		//Rasterize the primative and interpolate the vertex shader output
		FragmentBucket fragmentBucket;
		m_Rasterizer.Render(fragmentBucket,primitive);

		clock_t raster = clock() - rasterStart;
		clock_t fragmentStart = clock();


		numFragments += fragmentBucket.size();

		//Fragment shade
		for(Fragment fragment : fragmentBucket)
		{
			fragment.data = primitive.Interpolate(fragment.baryX,fragment.baryY,fragment.baryZ);
			m_pFragmentShader->Execute(m_pActiveBuffer,fragment);
		}
		clock_t fragment = clock() - fragmentStart;
		clock_t end = clock() - start;
		totalEnd += end;
		totalVertex+= vertexTime;
		totalRaster+= raster;
		totalFragment+= fragment;
	}

	printf("Total time = %f Vertex(%f) + raster(%f) + fragment(%f)\n",((float)totalEnd)/CLOCKS_PER_SEC,((float)totalVertex)/CLOCKS_PER_SEC,((float)totalRaster)/CLOCKS_PER_SEC,((float)totalFragment)/CLOCKS_PER_SEC );
	//m_pActiveBuffer->DrawDepth();
	printf("Number of fragments: %d \n",numFragments);
}

