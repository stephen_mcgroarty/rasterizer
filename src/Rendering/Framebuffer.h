#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H
#include "Core/Common.h"
#include "Maths/Maths.h"

class Framebuffer
{
public:
	Framebuffer(unsigned int width,unsigned int height);
	~Framebuffer();



	const Mat4f & GetBufferCoordMatrix() const { return m_BufferCoordMatrix; }
	void SetPixel(unsigned int x,unsigned int y,RGBColour colour);

	void Clear(int clearColour = 0x00);
	unsigned int GetHeight() const { return m_Height; }
	unsigned int GetWidth () const { return m_Width;  }



	void DrawDepth()
	{
		for(unsigned int x = 0; x < m_Width; ++x)
		{
			for(unsigned int y = 0; y < m_Height; ++y)
			{
				SetPixel(x,y,Vector4fToRGBColour(Vector4f(m_pDepthBuffer[x][y],m_pDepthBuffer[x][y] ,m_pDepthBuffer[x][y],1.0f)));
			}
		}
	}
	RGBColour * GetData() { return m_pData; }

	float **m_pDepthBuffer;

private:
	RGBColour * m_pData;


	//Transforms from fustrum space (-1->1 -1->1) to (0->width 0->height)
	Mat4f m_BufferCoordMatrix;
	const unsigned int m_Width,m_Height;
};

#endif