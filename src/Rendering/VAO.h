#ifndef VAO_H
#define VAO_H
#include <vector>
#include "Maths/Maths.h"
#include "Vertex.h"

class VAO
{
public:
	VAO(){}
	VAO(const std::vector<Vertex> & v): m_Vertices(v) {}

	const std::vector<Vertex> & GetVertices() const { return m_Vertices; }
	void Print()
	{
		for(auto v : m_Vertices)
		{
			v.Print();
		}
	}
private:
	std::vector<Vertex> m_Vertices;
};


#endif