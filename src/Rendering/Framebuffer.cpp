#include "Rendering/Framebuffer.h"
#include <memory.h>

Framebuffer::Framebuffer(unsigned int width,unsigned int height): m_Width(width),m_Height(height)
{
	m_pData = new RGBColour[m_Width*m_Height];
	m_pDepthBuffer = new float*[m_Width];
	for(unsigned int x = 0; x < m_Width; ++x)
	{	
		m_pDepthBuffer[x] = new float[m_Height]; 
	}
	Clear();

	float data[] = 
	{
		(m_Width/2.0f), 0, 0, (m_Width/2.0f),
		0, (m_Height/2.0f), 0, (m_Height/2.0f),
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	m_BufferCoordMatrix = Mat4f(data);
	//m_BufferCoordMatrix = m_BufferCoordMatrix.Transpose();
}

Framebuffer::~Framebuffer()
{ 
	if( m_pData ) delete[] m_pData;
}


void Framebuffer::Clear(int clearColour )
{
	memset(m_pData,clearColour,m_Width*m_Height*sizeof(RGBColour));	

	for(unsigned int x = 0; x < m_Width; ++x)
	{	
		memset(m_pDepthBuffer[x],0.0f,m_Height*sizeof(float)); 
	}
}


void Framebuffer::SetPixel(unsigned int x,unsigned int y,RGBColour colour )
{
	unsigned int index = y*m_Width + x;
	m_pData[index] = colour;
}
