#ifndef OBJ_LOADER_H
#define OBJ_LOADER_H
#include "VAO.h"
#include <vector>
#include <string>


// V = vertex, T = texture coordinate, N = Normal 
enum FaceFormat{
	V_ONLY,
	VT,
	VTN,
	VN,
	FORMAT_NULL
};


class ObjectLoader
{
public:

	static VAO * LoadObject(const std::string &filePath);

private:
	static void GenerateNormals(const std::vector<float> &verts,std::vector<float>& outNormals);
	static void RemoveSlashes(std::string &);


	

	//returns what format the faces are in
	static FaceFormat HandleFace(const std::string &);

};


#endif