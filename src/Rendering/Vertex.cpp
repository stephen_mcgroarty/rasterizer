#include "Vertex.h"


Vertex::Vertex(const Vector4f & pos): m_Position(pos) {}

Vertex::Vertex(const Vector3f & pos)
{

	float data[4] = {pos.GetData()[0],pos.GetData()[1],pos.GetData()[2],1};

	m_Position = Vector4f(data);
}
