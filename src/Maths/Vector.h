#ifndef VECTOR_H
#define VECTOR_H
#include <cmath>
#include <memory.h>
//#include "Maths/Matrix.h"
#include "Core/Common.h"

template<typename T,unsigned int length>
class Vector
{
public:
	Vector(T d1 = 0x0,T d2= 0x0,T d3= 0x0,T d4= 0x0);
	Vector(const T * data) { memcpy(m_Data,data,sizeof(T)*length);}

	const T * GetData() const { return m_Data; }
	T * GetData() { return m_Data; }
	inline T Magnitude() const;



	void Normalize();
	Vector<T,length> GetNormalized() const;
	T & operator[](unsigned int index) { return m_Data[index]; }
	const T & operator[](unsigned int index) const { return m_Data[index];}

	template<unsigned int otherLength> 
	Vector(const Vector<T,otherLength> & other);


	//For testing
	void Print() const 
	{
		printf("(");
		for(unsigned int index = 0; index < length; ++index)
		{
			printf("%f",m_Data[index]);

			if(index != length-1)
				printf(",");
		}
		printf(")\n");
	}


	Vector<T,length> Cross(const Vector<T,length> & other) const;

	T Dot(const Vector<T,length> & other) const;


	Vector<T,length> operator*(T scalar) const;
	void operator *=    (T scalar);


	Vector<T,length> operator+ (const Vector<T,length> & other) const;
	Vector<T,length> operator- (const Vector<T,length> & other) const;


	//Vector<T,length> operator* (const Matrix<T,length,length> & matrix) const;
	Vector<T,length> operator* (const Vector<T,length> & other) const;
	Vector<T,length> operator/ (const Vector<T,length> & other) const;


	void operator+=(const Vector<T,length> & other);
	void operator-=(const Vector<T,length> & other);
	void operator*=(const Vector<T,length> & other);
	void operator/=(const Vector<T,length> & other);
private:
	T m_Data[length];
};



template<typename T,unsigned int length>
Vector<T,length>::Vector(T d1 ,T d2,T d3,T d4)
{
	T data[4] = {d1,d2,d3,d4};
	memcpy(m_Data,data,sizeof(T)*length);
}


template<typename T,unsigned int length>
template<unsigned int otherLength>
Vector<T,length>::Vector(const Vector<T,otherLength> & other)
{
	if( otherLength >= length )
	{
		memcpy(m_Data,other.GetData(),sizeof(T)*length);
	} 
	else
	{
		memset(m_Data,0,sizeof(T)*length);
		memcpy(m_Data,other.GetData(),sizeof(T)*otherLength);
	}
}




template<typename T,unsigned int length>
inline T Vector<T,length>::Magnitude() const 
{
	T sum;
	for(unsigned int iElem = 0; iElem < length; ++iElem)
	{
		if( iElem == 0 )
			sum = m_Data[iElem]*m_Data[iElem];
		else 
			sum += m_Data[iElem]*m_Data[iElem];
	}


	return sqrt(sum);
}


template<typename T,unsigned int length>
void Vector<T,length>::Normalize()
{
	T size = this->Magnitude();

	for(unsigned int iElem = 0; iElem < length; ++iElem)
	{
		m_Data[iElem] /= size;
	}

}


template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::GetNormalized() const
{
	T size = this->Magnitude();

	T data[length];
	memcpy(data,m_Data,length*sizeof(T));


	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		data[iElem] /= size;
	}

	return Vector<T,length>(data);
}



template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::Cross(const Vector<T,length> & other) const
{

	if( length != 3)
	{
		return Vector<T,length>();
	}

	T data[3];
	data[0] = m_Data[1] * other.m_Data[2] - m_Data[2] * other.m_Data[1];
	data[1] = m_Data[0] * other.m_Data[2] - m_Data[2] * other.m_Data[0];
	data[2] = m_Data[0] * other.m_Data[1] - m_Data[1] * other.m_Data[0];

	return Vector(data);
}


template<typename T,unsigned int length>
T Vector<T,length>::Dot(const Vector<T,length> & other) const
{
	T sum;

	for(unsigned int iElem = 0; iElem < length; ++iElem)
	{
		if( iElem == 0 )
		{
			sum = m_Data[iElem]*other.m_Data[iElem];
		}
		else
		{
			sum += m_Data[iElem]*other.m_Data[iElem];
		}
	}
	return sum;
}

template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::operator*(T scalar) const
{
	T data[length];
	memcpy(data,m_Data,length*sizeof(T));


	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		data[iElem] *= scalar;
	}


	return Vector<T,length>(data);
}
	

template<typename T,unsigned int length>
void Vector<T,length>::operator *=(T scalar) 
{
	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		m_Data[iElem] *= scalar;
	}
}

template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::operator+ (const Vector<T,length> & other) const
{

	T data[length];
	memcpy(data,m_Data,length*sizeof(T));

	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		data[iElem] += other.m_Data[iElem];
	}

	return Vector<T,length>(data);
}

template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::operator- (const Vector<T,length> & other) const
{

	T data[length];
	memcpy(data,m_Data,length*sizeof(T));

	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		data[iElem] -= other.m_Data[iElem];
	}

	return Vector<T,length>(data);
}



template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::operator* (const Vector<T,length> & other) const
{	
	T data[length];
	memcpy(data,m_Data,length*sizeof(T));

	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		data[iElem] *= other.m_Data[iElem];
	}

	return Vector<T,length>(data);
}

template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::operator/ (const Vector<T,length> & other) const
{
	T data[length];
	memcpy(data,m_Data,length*sizeof(T));

	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		data[iElem] /= other.m_Data[iElem];
	}

	return Vector<T,length>(data);
}

template<typename T,unsigned int length>
void Vector<T,length>::operator+=(const Vector<T,length> & other)
{
	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		m_Data[iElem] += other.m_Data[iElem];
	}
}


template<typename T,unsigned int length>
void Vector<T,length>::operator-=(const Vector<T,length> & other)
{
	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		m_Data[iElem] -= other.m_Data[iElem];
	}
}




template<typename T,unsigned int length>
void Vector<T,length>::operator*=(const Vector<T,length> & other)
{
	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		m_Data[iElem] *= other.m_Data[iElem];
	}
}

template<typename T,unsigned int length>
void Vector<T,length>::operator/=(const Vector<T,length> & other)
{
	for(unsigned int iElem =0; iElem<length; ++iElem)
	{
		m_Data[iElem] /= other.m_Data[iElem];
	}
}


/*
template<typename T,unsigned int length>
Vector<T,length> Vector<T,length>::operator* (const Matrix<T,length,length> & matrix) const
{
	T temp[length];
	memset(temp,0,length*sizeof(T));
	const T * matrixData = matrix.GetData();
	//For each column in the matrix & in the returned vector...
	for(unsigned int iColumn = 0; iColumn < length; ++iColumn)
	{
		//For each elem in row[iColumn] in the matrix & element in this vector...
		for(unsigned int iElem = 0; iElem < length; ++iElem)
		{
			temp[iColumn] += m_Data[iElem]*matrixData[iElem*length + iColumn];
		}
	}
	return Vector<T,length>(temp);
}*/

#endif