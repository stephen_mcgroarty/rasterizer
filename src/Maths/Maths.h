#ifndef MATHS_H
#define MATHS_H
#include <cmath>

#include "Vector.h"
#include "Matrix.h"
#include "Core/Common.h"

typedef Vector<FLOAT,4>        Vector4f; 
typedef Vector<int,4>          Vector4i;
typedef Vector<unsigned int,4> Vector4ui;

typedef Vector<FLOAT,3>        Vector3f; 
typedef Vector<int,3>          Vector3i;
typedef Vector<unsigned int,3> Vector3ui;

typedef Vector<FLOAT,2>        Vector2f;
typedef Vector<int,2>          Vector2i;
typedef Vector<unsigned int,2> Vector2ui;

typedef Matrix<FLOAT,4,4> Mat4f;
typedef Matrix<FLOAT,3,3> Mat3f;
typedef Matrix<FLOAT,2,2> Mat2f;

typedef Matrix<int,4,4> Mat4i;
typedef Matrix<int,3,3> Mat3i;
typedef Matrix<int,2,2> Mat2i;

typedef Matrix<bool,4,4> Mat4b;
typedef Matrix<bool,3,3> Mat3b;
typedef Matrix<bool,2,2> Mat2b;


inline float max(float n1,float n2)
{
	if( n1 > n2 ) return n1;
	return n2;
}

inline float min(float n1,float n2)
{
	if( n1 < n2 ) return n1;
	return n2;
}


inline float interpolate(float vMin,float vMax,float gradient)
{
	return vMax - (vMax -vMin)*max(min(gradient,1.0f),0.0f);	
} 



//Takes a vector representing the RGBA colours with it's values between 0-1.0f
inline RGBColour Vector4fToRGBColour(const Vector4f & colourVector)
{	
	RGBColour colour;

	colour.red   = max(min(colourVector[0],1.0f), 0.0f)  *  255;
	colour.green = max(min(colourVector[1],1.0f), 0.0f)  *  255;
	colour.blue  = max(min(colourVector[2],1.0f), 0.0f)  *  255;
	colour.alpha = max(min(colourVector[3],1.0f), 0.0f)  *  255;

	return colour;
}




template<typename T>
Matrix<T,4,4> Translate(T x,T y,T z)
{
	T f[] = { 1, 0, 0, x,
			  0, 1, 0, y,
			  0, 0, 1, z,
			  0, 0, 0, 1
			};

	return Matrix<T,4,4>(f);
}


template<typename T>
Matrix<T,4,4> Scale(T x,T y,T z)
{
	T f[] = { x, 0, 0, 0,
			  0, y, 0, 0,
			  0, 0, z, 0,
			  0, 0, 0, 1
			};

	return Matrix<T,4,4>(f);

}

template<typename T>
Matrix<T,4,4> Rotate(float angle,const Vector<T,3> & axis)
{
	T axisX = axis.GetData()[0];
	T axisY = axis.GetData()[1];
	T axisZ = axis.GetData()[2];


	T cosT = cos(angle);
	T sinT = sin(angle);

	T data[] =
		{
			cosT + (1 -cosT)*axisX*axisX  , (1 - cosT)*axisX*axisY - sinT*axisZ  , (1-cosT)*axisX*axisZ + sinT*axisY  , 0,

			(1 - cosT)*axisX*axisY + sinT*axisZ  , cosT + (1 -cosT)*axisY*axisY  ,(1-cosT)*axisY*axisZ - sinT*axisX  , 0,

			(1 - cosT)*axisX*axisZ - sinT*axisY  , (1 - cosT)*axisY*axisZ + sinT*axisX, cosT + (1 - cosT)*axisZ*axisZ, 0,

			0,  0 , -1, 0
		};


	return Matrix<T,4,4>(data);
}


template<typename T>
Matrix<T,4,4> Perspective(T fov,T aspect, T near, T far)
{
    T m00 = 1 / (aspect*tan(0.5f*fov));
    T m11 = 1 / tan(0.5f*fov);
    T m22 = (-near-far)/(near-far);
    T m23 = (2*near*far)/(near-far);

	T data[] = 
	{
		m00,     0,   0,   0,
		0  ,   m11,   0,   0,
		0  ,     0, m22,  m23, 
		0  ,     0, -1,   0
	};

	return Matrix<T,4,4>(data);
}


#endif