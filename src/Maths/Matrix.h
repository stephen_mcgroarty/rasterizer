#ifndef MATRIX_H
#define MATRIX_H
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "Maths/Vector.h"
#include <Core/Common.h>


template<typename T,unsigned int width,unsigned int height>
class Matrix
{
public:
	//Identity when Matrix(1.0f)
	Matrix(T identiyValue);
	Matrix();

	Matrix(const Matrix<T,width,height> & cpy);
	Matrix(T * data);


	~Matrix();

	template<unsigned int otherDimention>
	Matrix(const Matrix<T,otherDimention,otherDimention> & otherMatrix);


	Matrix<T,height,width> Transpose() const;

	Matrix<T,width,height> operator*(T scalar) const;
	Matrix<T,width,height> operator*(const Matrix<T,width,height> & other) const;
	Matrix<T,width,height> operator+(const Matrix<T,width,height> & other) const;
	Matrix<T,width,height> operator-(const Matrix<T,width,height> & other) const;


	Vector<T,height> operator*(const Vector<T,height> & vector) const;


	void operator*=(T scalar);
	void operator*=(const Matrix<T,width,height> & other) ;
	void operator+=(const Matrix<T,width,height> & other) ;
	void operator-=(const Matrix<T,width,height> & other) ;
	
	void operator=(const Matrix<T,width,height> & other);

	void Print() const;


	const T* GetData() const { return m_Data;}

private:
	T m_Data[width*height];

};



template<typename T,unsigned int width,unsigned int height>
template<unsigned int otherDimention>
Matrix<T,width,height>::Matrix(const Matrix<T,otherDimention,otherDimention> & otherMatrix)
{
	memset(m_Data,0,sizeof(T)*height*width);

	unsigned int lowestHeight = otherDimention >= height ? height : otherDimention;

	for(unsigned int h = 0; h < lowestHeight; ++h)
	{
		unsigned int indexMatOne = width*h;
		unsigned int indexMatTwo = otherDimention*h;
		memcpy(m_Data + indexMatOne,otherMatrix.GetData()+indexMatTwo,sizeof(T)*otherDimention);
	} 
}



template<typename T,unsigned int width,unsigned int height>
Matrix<T,height,width> Matrix<T,width,height>::Transpose() const
{

	T data[width*height];
	for(unsigned int row = 0; row < height; ++row)
	{
		for(unsigned int column =0; column < width; ++column)
		{
			unsigned int index = row*width + column;
			unsigned int indexMatTwo = column*height + row;

			data[indexMatTwo] = m_Data[index];
		}
	}


	return Matrix<T,height,width>(data);

}

template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height>::Matrix()
{
	memset(m_Data,0,width*height*sizeof(T)); 
}




template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height>::Matrix(T * data)
{
	memcpy(m_Data,data,width*height*sizeof(T)); 
}


template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height>::Matrix(T identiyValue)
{
	memset(m_Data,0,width*height*sizeof(T)); 

	unsigned int iWidth =0,iHeight = 0;
	while(iWidth < width && iHeight < height)
	{

		unsigned int index = width*iHeight + iWidth;
		m_Data[index] = identiyValue;

		++iWidth;
		++iHeight;
	}
}




template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height>::Matrix(const Matrix<T,width,height> & cpy)
{
	memcpy(m_Data,cpy.m_Data,width*height*sizeof(T)); 	
}




template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height>::~Matrix(){}




template<typename T,unsigned int width,unsigned int height>
void Matrix<T,width,height>::Print() const
{
	for (int iHeight = 0; iHeight < height; ++iHeight)
	{
		for(unsigned int iWidth = 0; iWidth < width; ++iWidth)
		{

			unsigned int index = width*iHeight + iWidth;
			printf("%f ",m_Data[index]);	
		}
		printf("\n");
	}
}




template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height> Matrix<T,width,height>::operator*(T scalar) const
{

	Matrix<T,width,height> result(m_Data);

	for(unsigned int index = 0; index < width*height; ++index)
	{
		result.m_Data[index] *= scalar;
	}

	return result;
}

template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height> Matrix<T,width,height>::operator+(const Matrix<T,width,height> & other) const
{
	Matrix<T,width,height> result(m_Data);

	for(unsigned int index = 0; index < width*height; ++index)
	{
		result.m_Data[index] += other.m_Data[index];
	}

	return result;
}


template<typename T,unsigned int width,unsigned int height>
Matrix<T,width,height> Matrix<T,width,height>::operator-(const Matrix<T,width,height> & other) const
{
	Matrix<T,width,height> result(m_Data);

	for(unsigned int index = 0; index < width*height; ++index)
	{
		result.m_Data[index] -= other.m_Data[index];
	}

	return result;
}





template<typename T,unsigned int width,unsigned int height>
void Matrix<T,width,height>::operator*=(T scalar) 
{
	for(unsigned int index = 0; index < width*height; ++index)
	{
		m_Data[index] *= scalar;
	}
}




template<typename T,unsigned int width,unsigned int height>
void Matrix<T,width,height>::operator+=(const Matrix<T,width,height> & other)
{
	for(unsigned int index = 0; index < width*height; ++index)
	{
		m_Data[index] += other.m_Data[index];
	}
}



template<typename T,unsigned int width,unsigned int height>
void Matrix<T,width,height>::operator-=(const Matrix<T,width,height> & other)
{
	for(unsigned int index = 0; index < width*height; ++index)
	{
		m_Data[index] -= other.m_Data[index];
	}
}


template<typename T, unsigned int width,unsigned int height>
void Matrix<T, width, height>::operator*=(const Matrix<T,width,height> & other)
{
	Matrix<T,width,height> result;
	
	//For each row in matrix one
	for(unsigned int iRow = 0; iRow < height; ++iRow)
	{
		//For each column in matrix two
		for (unsigned int iColumn = 0; iColumn < height; ++iColumn)
		{
			unsigned int iElemRow =0,iElemColumn = 0;
			while( iElemColumn < height && iElemRow < width )
			{
				unsigned int resultIndex = iRow*width + iColumn;

				unsigned int matrixOneIndex = iRow*width + iElemRow;
				unsigned int matrixTwoIndex = iElemColumn*height + iColumn;

				result.m_Data[resultIndex] += (m_Data[matrixOneIndex] * other.m_Data[matrixTwoIndex]);

				++iElemRow;
				++iElemColumn;
			}
		}
	}

	*this = result;

}




template<typename T, unsigned int width,unsigned int height>
Matrix<T,width,height> Matrix<T, width, height>::operator*(const Matrix<T,width,height> & other) const
{
	Matrix<T,width,height> result;
	
	//For each row in matrix one
	for(unsigned int iRow = 0; iRow < height; ++iRow)
	{
		//For each column in matrix two
		for (unsigned int iColumn = 0; iColumn < height; ++iColumn)
		{
			unsigned int iElemRow =0,iElemColumn = 0;
			while( iElemColumn < height && iElemRow < width )
			{
				unsigned int resultIndex = iRow*width + iColumn;

				unsigned int matrixOneIndex = iRow*width + iElemRow;
				unsigned int matrixTwoIndex = iElemColumn*height + iColumn;

				result.m_Data[resultIndex] += (m_Data[matrixOneIndex] * other.m_Data[matrixTwoIndex]);

				++iElemRow;
				++iElemColumn;
			}
		}
	}
	return result;
}


template<typename T,unsigned int width,unsigned int height>
Vector<T,height> Matrix<T,width,height>::operator*(const Vector<T,height> & vector) const
{
	T temp[width];
	memset(temp,0,width*sizeof(T));

	const T * vectorData = vector.GetData();
	//For each column in the matrix & in the returned vector...
	for(unsigned int iColumn = 0; iColumn < height; ++iColumn)
	{
		//For each elem in row[iColumn] in the matrix & element in this vector...
		for(unsigned int iElem = 0; iElem < width; ++iElem)
		{
			temp[iColumn] += vectorData[iElem]*m_Data[iColumn*width + iElem];
		}
	}
	return Vector<T,width>(temp);
}



template<typename T,unsigned int width, unsigned int height>
void Matrix<T,width,height>::operator=(const Matrix<T,width,height> & other)
{
	memcpy(m_Data,other.m_Data,width*height*sizeof(T));
}

#endif